<?php
    //NOTE: Call database connection
    require_once('connection.php');

    //NOTE: jika Con&Act ada maka ....
    if (isset($_GET['controller']) && isset($_GET['action'])) {
        $controller = $_GET['controller'];
        $action = $_GET['action'];
    } else {
        $controller = 'pages';
        $action = 'home';
    }

    require_once('views/layout.php');
?>