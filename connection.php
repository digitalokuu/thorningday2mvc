<?php
  class Db {
    private static $instance = NULL;

    //NOTE: Can't call Outside
    private function __construct() {}

    private function __clone() {}

    public static function getInstance() {
      if (!isset(self::$instance)) {
        //NOTE: Ubah Mode & Tampil Error
        $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
        self::$instance = new PDO('mysql:host=localhost;dbname=ThorningDay2_MVC', 'root', '', $pdo_options);
      }
      return self::$instance;
    }
  }
?>