<?php
  function call($controller, $action) {
    //NOTE: Require File controller
    require_once('controllers/' . $controller . '_controller.php');

    //NOTE: New Instance Controller
    switch($controller) {
        case 'pages':
        $controller = new PagesController();
      break;
      case 'info':
        require_once('models/info.php');
        require_once ('services/eloquents/InputEloquents.php');
        $controller = new InfoController();
      break;
    }

    ///NOTE: call Action
    $controller->{ $action }();
  }

  //NOTE: list of available action
  $controllers = array('pages' => ['home', 'error'],
                       'info' => ['home', 'error','create','createprocess']);

  //NOTE: Check page, if doesn't exist redirect to error page
  if (array_key_exists($controller, $controllers)) {
    if (in_array($action, $controllers[$controller])) {
      call($controller, $action);
    } else {
      call('pages', 'error');
    }
  } else {
    call('pages', 'error');
  }
?>