<?php
  class Post {
    //NOTE: make var public to everyone
    public $id;
    public $name;
    public $addr;
    public $phone;

    public function __construct($id, $name, $addr,$phone) {
      $this->id      = $id;
      $this->name  = $name;
      $this->addr = $addr;
      $this->phone = $phone;
    }

    public static function all() {
      $list = [];
      $db = Db::getInstance();
      $req = $db->query('SELECT * FROM info');

      //NOTE: Take all record from databse
      foreach($req->fetchAll() as $info) {
        $list[] = new Post($info['id'], $info['name'], $info['addr'],$info['phone']);
      }

      return $list;
    }
  }
?>