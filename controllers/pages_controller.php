<?php
    //NOTE: Initiate class
    class PagesController {

    //NOTE: Controller PAGE action HOME
    public function home() {
        $first_name = 'Jon';
        $last_name  = 'Snow';
        require_once('views/pages/home.php');
    }

    //NOTE: Controller PAGE action ERROR
    public function error() {
        require_once('views/pages/error.php');
    }
    }
?>