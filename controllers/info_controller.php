<?php
    //NOTE: Initiate class
    class InfoController {
        public $interface;
        public function __construct(){
            $this->interface = new InputEloquents;

        }

    //NOTE: Controller POST action HOME
    public function home() {
        $posts = Post::all();
        // $first_name = 'Jonny';
        // $last_name  = 'Snowy';
        require_once('views/Info/home.php');
    }

    public function create() {
        // $first_name = 'Jonny';
        // $last_name  = 'Snowy';
        require_once('views/Info/create.php');
    }

    public function createprocess() {
        $createprocess = [
                          'name' =>$_POST['name'],
                          'addr' =>$_POST['addr'],
                          'phone'=>$_POST['phone']];
        require_once('views/Info/create.php');
        $this->interface->post_input($createprocess);
    }

    //NOTE: Controller POST action ERROR
    public function error() {
        require_once('views/info/error.php');
    }
    }
?>